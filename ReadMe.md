# LogZip

## LogZip Amacı
Bu projenin amacı oluşturulan bir .log uzantılı dosyaya yollanan verinin, dosya boyutu 1GB'ı aştığı zaman, sıkıştırılıp kaydedilmesi.

## LogZip İndirmek
LogZip projesini kullanabilmek için proje içerisindeki headers ve sources dosyalarını projenize eklemeniz gerekir. Testleri yapabilmek için [Gtest'e](https://github.com/google/googletest) sahip olmalısınız.

## LogZip Örnek

Öncelikle 
```c++
#include<"loguruzip.h">
```

LogZip start() and stop()
```c++
Loguruzip obj;
obj.start(argc,argv);

obj.stop();
```
Logzip getFilesize() long türünde bir sayı döndürür
```c++
std::cout<<obj.getFilesize();
```
LogZip filename string türünde bir değişkendir. Başlangıçta "everything.log" değerini saklar, değiştirdiğimizde oluşacak dosya adı değişir.
```c++
obj.filename="newfile.log";

```




